﻿using androidmonkey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Utilities;
using System.Collections.Specialized;
using System.Collections;
using System.Threading;

namespace ReadFMA
{
    class Program
    {
        static void Usage()
        {
            Console.WriteLine("[-utilpath=adb fullpathname]");
            Console.WriteLine("[-label=x]");
            Console.WriteLine("-screenlock=true/false");
            Console.WriteLine("[-serial=adb serailname]");
            Console.WriteLine("-manufacture=");
            Console.WriteLine("-hubname="); 
            Console.WriteLine("-hubport=");
            Console.WriteLine("-FMM=true/false");
            Console.WriteLine("-googlecount=");
            Console.WriteLine("[-version=android verion]");
            Console.WriteLine("-setlanguage");
            Console.WriteLine("-removegoogleaccount");
            Console.WriteLine("");
            Console.WriteLine("");
        }

        protected static void WriteAt(string s, int x, int y)
        {
            try
            {
                Console.SetCursorPosition(x, y);
                if(!String.IsNullOrEmpty(s))
                    Console.Write(s);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
        }
        protected static int origRow = -1;
        protected static int origCol = -1;
        private static void drawTextProgressBar(int progress, int total)
        {
            if(origCol==-1 && origRow == -1)
            {
                origRow = Console.CursorTop;
                origCol = Console.CursorLeft;
            }
            int origR = Console.CursorTop;
            int origC = Console.CursorLeft;
            WriteAt("", origCol, origRow);
            //draw empty progress bar
            WriteAt("[", 0, origRow);
            WriteAt("]", 32, origRow);
            Console.CursorLeft = 1;
            float onechunk = 30.0f / total;

            //draw filled part
            int position = 1;
            for (int i = 0; i < onechunk * progress; i++)
            {
                Console.BackgroundColor = ConsoleColor.Green;
                ConsoleColor temp = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.White;
                WriteAt("=", position++, origRow);
                Console.ForegroundColor = temp;
            }

            //draw unfilled part
            for (int i = position; i <= 31; i++)
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                WriteAt(" ", position++, origRow);
            }

            //draw totals
            Console.CursorLeft = 35;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("{0} of {1}, Percent:{2:0.00}%", progress,total, (100.0*progress)/total); //blanks at the end remove any excess
            WriteAt((origCol==-1 && origRow == -1)?"\n":"", origC, origR);
        }

        static Boolean IgnoreScreenLock()
        {
            Boolean ret = true;
            string sConfigName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "readFMA.ini");
            if (!File.Exists(sConfigName)) return ret;
            IniFile ini = new IniFile(sConfigName);
            ret = Convert.ToBoolean(ini.GetString("ignore", "ignorescreenlock", "true"));

            return ret;
        }


        static int CheckMcafeeLock(StringBuilder sb, adbHelper adb, String sID)
        {
            String ssm = String.Format(" {0} shell pm list packages -s", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
            if (adb.runAdb(ssm) != ERRORCODE.SUCCESS)
            {
                return ERRORCODE.ERROR_ADBRUNFAILED;
            }
            string[] sarraylg = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            Boolean bFindpackage = false;
            foreach (string s in sarraylg)
            {
                if (s.IndexOf("com.wsandroid.suite.lge") > 0)
                {
                    bFindpackage = true;
                    break;
                }
            }
            if (!bFindpackage)
            {
                Console.WriteLine("mcafeelockable=false");
                Console.WriteLine("mcafeelock=false");
                return ERRORCODE.SUCCESS;
            }
            else
            {
                Console.WriteLine("mcafeelockable=true");
            }

            String sFileName = Path.GetTempFileName();
            File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.SecuritySettings);
            ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/SecuritySettings.jar", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
            //adb.runAdb(ssm);
            adbHelper.LogIt(ssm);
            if (adb.runAdb(ssm) != ERRORCODE.SUCCESS)
            {
                sb.Append("M");
                Console.WriteLine(sb);
                return ERRORCODE.ERROR_ADBRUNFAILED;
            }
            sb.Append("N");
            try
            {
                File.Delete(sFileName);
            }
            catch (Exception)
            {
            }
            int nRetry = 3;
        LABELRETRY:
            ssm = String.Format(" {0} shell uiautomator runtest /data/local/tmp/SecuritySettings.jar -c com.fd.fma.SecuritySettings#testCheckMcafeeLock", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
            if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
            {
                sb.Append("O");
                Console.WriteLine(sb);
                return ERRORCODE.ERROR_ADBRUNFAILED;
            }
            sarraylg = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            sb.Append("k");
            foreach (string s in sarraylg)
            {
                string[] infos = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (infos != null && infos.Length == 2)
                {
                    if (string.Compare(infos[0], "mcafeelock", true) == 0)
                    {
                        Console.WriteLine(s);
                        break;
                    }
                    else if (string.Compare(infos[0], "error", true) == 0)
                    {
                        if (--nRetry == 0) break;
                        goto LABELRETRY;
                    }
                }
            }
            if (nRetry <= 0) return ERRORCODE.ERROR_TIMEOUT;

            return ERRORCODE.SUCCESS;
        }

        static Boolean GetCheckScreenLock(string skey)
        {
            Boolean ret = false;
            string sConfigName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "readFMA.ini");
            if (!File.Exists(sConfigName)) return ret;
            IniFile ini = new IniFile(sConfigName);
            if(!String.IsNullOrEmpty(skey))
                ret = Convert.ToBoolean(ini.GetString("checkscreenlock", skey, "false"));
            return ret;
        }

        static void SendCurlInfo(Boolean bSetOn)
        {
            String curl = Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "curl.exe");
            if (File.Exists(curl))
            {
                // error code
                adbHelper.LogIt(String.Format("[Label_{0}]: start send Screenlock flag to UI\n", adbHelper._label));
                String args = String.Format("http://localhost:1210/modifyinfo --data-urlencode label={0} --data-urlencode xpath=\"/labelinfo/runtime/screenon\" --data-urlencode value=\"{1}\" --get", adbHelper._label, bSetOn);
                adbHelper curlexe = new adbHelper(curl);
                int err = curlexe.runExe(curl, args, System.AppDomain.CurrentDomain.BaseDirectory);
                adbHelper.LogIt(String.Format("[Label_{0}]: end send error code to UI ret-%d\n", adbHelper._label, err));
            }
        }

        static int tapScreenByNode(adbHelper adb, String serialno, string sNode, int xx, int yy)
        {
            sNode = string.Format("//node[translate(@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz') = '{0}']", sNode.ToLower());
            adbHelper.LogIt("tapScreenByNode: ++ " + sNode);
            int ret = 1;
            int err = 1;
            string[] lines;
            string s="";
            try
            {
                int nretry = 5;
                bool bsucced = false;
                while (!bsucced && nretry-- >= 0)
                {
                    s = System.IO.Path.GetRandomFileName();
                    s = System.IO.Path.ChangeExtension(s, ".xml");
                    adbHelper.LogIt($"dump xml file: {s}");
                    lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell uiautomator dump /sdcard/{s}", out err, null, 5*60*1000);
                    foreach (string sline in lines)
                    {
                        adbHelper.LogIt($"dump xml return: {sline}");
                        if (sline.StartsWith("UI hierchary dumped"))
                        {
                            bsucced = true;
                            break;
                        }
                    }
                    if (!bsucced)
                        Thread.Sleep(3*1000);
                }

                lines = adbHelper.runExe_v2(adb.GetAdbExe(), $"{serialno} pull /sdcard/{s} \"{System.IO.Path.Combine(System.IO.Path.GetTempPath(), s)}\"", 
                    out err, null, 5*60*1000);

                string localPath = string.Format("{0}", System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
                adbHelper.LogIt($"get local path:{localPath}");
                if (!File.Exists(localPath))
                {
                    adbHelper.LogIt("dump file failed, so user default position.");
                    adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {xx + 10} {yy + 10}", out err);
                    return 0;
                }

                XmlDocument doc = new XmlDocument();
                adbHelper.LogIt("start load xml:" + localPath);
                doc.Load(localPath);
                adbHelper.LogIt("start find node:" + sNode);
                XmlNode n = doc.SelectSingleNode(sNode);
                adbHelper.LogIt("find node:" + sNode);
                if (n != null)
                {
                    XmlAttribute a = n.Attributes["bounds"];
                    if (a != null)
                    {
                        Regex r = new Regex(@"\[(\d+),(\d+)\]\[(\d+),(\d+)\]");
                        Match m = r.Match(a.Value);
                        if (m.Success)
                        {
                            int x = -1, y = -1;
                            adbHelper.LogIt("start get x and y");
                            if (Int32.TryParse(m.Groups[1].Value, out x) && Int32.TryParse(m.Groups[2].Value, out y))
                            {
                                adbHelper.LogIt("start get x and y");
                                lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {x + 10} {y + 10}", out err);
                                ret = 0;
                            }
                        }

                    }
                }
                else
                {
                    adbHelper.LogIt($"tapScreenByNode: not found: {sNode}");
                    adbHelper.LogIt(System.IO.File.ReadAllText(localPath));
                    ret = 4;
                }
                System.IO.File.Delete(System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
            }
            catch (Exception e) {
                adbHelper.LogIt(e.Message);
                adbHelper.LogIt(e.StackTrace);
                ret = 3;
            }

            adbHelper.LogIt(string.Format("tapScreenByNode: -- ,ret:{0}", ret));
            return ret;

        }

        static int tapScreenByNodeV2(adbHelper adb, String serialno, string sNode, int xx, int yy)
        {
           // sNode = string.Format("//node[translate(@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz') = '{0}']", sNode.ToLower());
            adbHelper.LogIt("tapScreenByNodeClass: ++ " + sNode);
            int ret = 1;
            int err = 1;
            string[] lines;
            string s = "";
            try
            {
                int nretry = 5;
                bool bsucced = false;
                while (!bsucced && nretry-- >= 0)
                {
                    s = System.IO.Path.GetRandomFileName();
                    s = System.IO.Path.ChangeExtension(s, ".xml");
                    adbHelper.LogIt($"dump xml file: {s}");
                    lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell uiautomator dump /sdcard/{s}", out err, null, 5 * 60 * 1000);
                    foreach (string sline in lines)
                    {
                        adbHelper.LogIt($"dump xml return: {sline}");
                        if (sline.StartsWith("UI hierchary dumped"))
                        {
                            bsucced = true;
                            break;
                        }
                    }
                    if (!bsucced)
                        Thread.Sleep(3 * 1000);
                }

                lines = adbHelper.runExe_v2(adb.GetAdbExe(), $"{serialno} pull /sdcard/{s} \"{System.IO.Path.Combine(System.IO.Path.GetTempPath(), s)}\"",
                    out err, null, 5 * 60 * 1000);

                string localPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), s);

                if (!File.Exists(localPath))
                {
                    adbHelper.LogIt("dump file failed, so user default position.");
                    adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {xx + 10} {yy + 10}", out err);
                    return 0;
                }

                XmlDocument doc = new XmlDocument();
                adbHelper.LogIt("start load xml:" + localPath);
                doc.Load(localPath);
                adbHelper.LogIt("start find node:" + sNode);
                XmlNode n = doc.SelectSingleNode(sNode);
                adbHelper.LogIt("find node:" + sNode);
                if (n != null)
                {
                    XmlAttribute a = n.Attributes["bounds"];
                    if (a != null)
                    {
                        Regex r = new Regex(@"\[(\d+),(\d+)\]\[(\d+),(\d+)\]");
                        Match m = r.Match(a.Value);
                        if (m.Success)
                        {
                            int x = -1, y = -1;
                            adbHelper.LogIt("start get x and y");
                            if (Int32.TryParse(m.Groups[1].Value, out x) && Int32.TryParse(m.Groups[2].Value, out y))
                            {
                                adbHelper.LogIt("start get x and y");
                                lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {x + 10} {y + 10}", out err);
                                ret = 0;
                            }
                        }

                    }
                }
                else
                {
                    adbHelper.LogIt($"tapScreenByNodeClass: not found: {sNode}");
                    adbHelper.LogIt(System.IO.File.ReadAllText(localPath));
                    ret = 4;
                }
                System.IO.File.Delete(System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
            }
            catch (Exception e)
            {
                adbHelper.LogIt(e.Message);
                adbHelper.LogIt(e.StackTrace);
                ret = 3;
            }

            adbHelper.LogIt(string.Format("tapScreenByNodeClass: -- ,ret:{0}", ret));
            return ret;

        }

        static int do_FactoryResetOPPOCPH1851(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH1851: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            // remove the DeviceAdmin popup message
            // ret = tapScreenByNode(adb, serialno, "//node[@text='OK']", 70, 1301);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset to Factory Settings", 32, 873);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Content & Settings", 32, 436);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 16, 1182);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                int r = tapScreenByNode(adb, serialno, "Erase Data", 16, 1182);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);


            return ret;
        }

        static int do_FactoryResetOPPOCPH1903(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH1903: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            // remove the DeviceAdmin popup message
           // ret = tapScreenByNode(adb, serialno, "//node[@text='OK']", 70, 1301);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset to Factory Settings", 72, 1558);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Content & Settings", 32, 436);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 16, 1182);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                int r = tapScreenByNode(adb, serialno, "Erase Data", 16, 1182);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);


            return ret;
        }

        static int do_FactoryResetHuaweiALEUL00(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetHuaweiALEUL00: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            // remove the DeviceAdmin popup message
            // ret = tapScreenByNode(adb, serialno, "//node[@text='OK']", 70, 1301);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Factory data reset", 36, 727);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNodeV2(adb, serialno, "//node[@class='android.widget.CheckBox']", 32, 397);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset phone", 30, 1032);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset phone", 360, 1032);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);


            return ret;
        }

        static int do_FactoryResetHuaweiALEL21(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetHuaweiALEL21: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            // remove the DeviceAdmin popup message
            // ret = tapScreenByNode(adb, serialno, "//node[@text='OK']", 70, 1301);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Factory data reset", 36, 1007);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNodeV2(adb, serialno, "//node[@class='android.widget.CheckBox']", 32, 362);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset phone", 30, 1056);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset phone", 30, 1056);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);


            return ret;
        }

        static int do_FactoryResetHuaweiPETL10(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetHuaweiPETL10: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            // remove the DeviceAdmin popup message
            // ret = tapScreenByNode(adb, serialno, "//node[@text='OK']", 70, 1301);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Factory data reset", 54, 1531);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNodeV2(adb, serialno, "//node[@class='android.widget.CheckBox']", 48, 590);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNodeV2(adb, serialno, "//node[@resource-id='com.android.settings:id/initiate_master_clear']", 45, 1584);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                int r = tapScreenByNodeV2(adb, serialno, "//node[@resource-id='com.android.settings:id/execute_master_clear']", 45, 1584);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);


            return ret;
        }
        static int do_FactoryResetOPPOCPH1983(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH1983: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            // remove the DeviceAdmin popup message
            ret = tapScreenByNode(adb, serialno, "OK", 60, 1291);
            Thread.Sleep(2 * 1000);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset to Factory Settings", 72, 1558);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Clear All Data", 72, 638);
                if (ret == 0)
                {
                    break;
                }
                if ( ret == 4)
                {
                    adbHelper.LogIt(string.Format("CPH1983 can't find clear all data button.start find \"erase all data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase All Data", 72, 638);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Clear Data", 0, 1828);
                if (ret == 0)
                {
                    break;
                }

                if (ret == 4)
                {
                    adbHelper.LogIt(string.Format("CPH1983 can't find clear data button.start find \"erase data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1828);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Clear Data", 0, 1828);
                if (ret == 0)
                {
                    break;
                }

                if ( ret == 4 )
                {
                    adbHelper.LogIt(string.Format("CPH1983 can't find clear data button.start find \"erase data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1828);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);


            return ret;
        }

        static int do_FactoryResetOPPOCPH2099(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH2099: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional settings", 210, 1118);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back up and reset", 72, 1679);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1592);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data", 72, 681);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1894);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("CPH2099 can't find Erase databutton.start find \"Erase Data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase data & remove eSIM profiles", 0, 1708);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1894);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("CPH2099 can't find Erase data button.start find \"Erase Data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase data & remove eSIM profiles", 0, 1708);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOCPH2199(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH2199: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional settings", 210, 966);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("CPH2199 can't find Additional settings.start find \"System settings\""));
                    ret = tapScreenByNode(adb, serialno, "System settings", 210, 966);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back up and reset", 72, 1983);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1651);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("CPH2199 can't find Erase all data (factory reset).start find \"Reset phone\""));
                    ret = tapScreenByNode(adb, serialno, "Reset phone", 72, 1651);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data", 72, 890);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data & remove eSIM profiles", 0, 1768); 
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("CPH2199 can't find Erase databutton.start find \"Erase Data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1894);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data & remove eSIM profiles", 0, 1924);
                
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("CPH2199 can't find Erase data button.start find \"Erase Data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1894);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOCPH2013(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH2013: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional Settings", 210, 1118);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back Up and Reset", 72, 1679);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1640);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Data", 72, 681);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOCPH2195(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH2195: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional Settings", 210, 1118);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back Up and Reset", 72, 1679);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1640);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Data", 72, 681);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOA001OP(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOA001OP: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional settings", 210, 1128);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back up and reset", 72, 1983);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1324);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data", 72, 887);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOAdditional(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOAdditional: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional settings", 210, 1128);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("device can't find Additional settings.start find \"System settings\""));
                    ret = tapScreenByNode(adb, serialno, "System settings", 210, 966);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back up and reset", 72, 1983);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1324);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("device can't find Erase all data (factory reset).start find \"Reset phone\""));
                    ret = tapScreenByNode(adb, serialno, "Reset phone", 72, 1651);
                    if (ret == 0)
                    {
                        break;
                    }
                    if (ret == 4)
                    {
                        //Erase All Content & Settings
                        adbHelper.LogIt(string.Format("device can't find Reset phone. start find \"Reset to Factory Settings\""));
                        ret = tapScreenByNode(adb, serialno, "Reset to Factory Settings", 72, 1651);
                        if (ret == 0 || ret == 4)
                        {
                            break;
                        }
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data", 72, 887);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1924);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("can't find Erase databutton.start find \"Erase Data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase data & remove eSIM profiles", 0, 1708);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1924);
                if (ret == 0)
                {
                    break;
                }
                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("can't find Erase databutton.start find \"Erase Data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase data & remove eSIM profiles", 0, 1708);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }
        static int do_FactoryResetOPPOA101OP(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOA101OP: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional settings", 210, 966);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back up and reset", 72, 1983);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1651);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data", 72, 890);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }
        static int do_FactoryResetOPPOCPH2135(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH2135: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional Settings", 210, 1118);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back Up and Reset", 72, 1679);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1640);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Data", 72, 681);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1924);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }
        static int do_FactoryResetOPPOCPH1919(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH1919: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            ret = tapScreenByNode(adb, serialno, "OK", 60, 1291);
            Thread.Sleep(2 * 1000);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional Settings", 210, 922);
                if (ret == 0)
                {
                    break;
                }

                if (ret == 4)
                {
                    adbHelper.LogIt(string.Format("CPH1819 can't find Additional Settings button.start find \"Additional Settings\""));
                    ret = tapScreenByNode(adb, serialno, "Additional settings", 210, 1075);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Back Up and Reset", 72, 1923);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1640);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Data", 72, 681);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1864);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                int r = tapScreenByNode(adb, serialno, "Erase Data", 0, 1864);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOCPH1877(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH1877: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional Settings", 210, 1116);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                //Back Up and Reset
                ret = tapScreenByNode(adb, serialno, "Back Up and Reset", 72, 1933);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                //Erase all data (factory reset)
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1640);
                if (ret == 0)
                {
                    break;
                }

                if (ret == 4)
                {
                    //Reset to Factory Settings
                    adbHelper.LogIt(string.Format("CPH1877 can't find Erase all data button.start find \"Erase all data (factory reset)\""));
                    ret = tapScreenByNode(adb, serialno, "Reset to Factory Settings", 48, 1308);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                //Erase All Data
                ret = tapScreenByNode(adb, serialno, "Erase All Data", 72, 681);
                if (ret == 0)
                {
                    break;
                }

                if (ret == 4)
                {
                    //Erase All Content & Settings
                    adbHelper.LogIt(string.Format("CPH1877 can't find Erase all data button.start find \"Erase All Data\""));
                    ret = tapScreenByNode(adb, serialno, "Erase All Content & Settings", 48, 654);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                //Erase Data
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1864);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                int r = tapScreenByNode(adb, serialno, "Erase Data", 0, 1864);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOCPH1943(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH1943: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            ret = tapScreenByNode(adb, serialno, "OK", 60, 1291);
            Thread.Sleep(2 * 1000);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional settings", 140, 1329);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                //Backup and Reset
                ret = tapScreenByNode(adb, serialno, "Back up and reset", 48, 1120);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                //Reset to Factory Settings
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 48, 1055);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase all data", 48, 446);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase data", 0, 1282);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                int r = tapScreenByNode(adb, serialno, "Erase data", 0, 1282);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPOCPH1875(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPOCPH1875: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;

            ret = tapScreenByNode(adb, serialno, "OK", 60, 1291);
            Thread.Sleep(2 * 1000);

            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am force-stop com.android.settings", out err);
            Thread.Sleep(2 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start com.android.settings/com.android.settings.Settings", out err);
            if (err != 0) return err;
            Thread.Sleep(5 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            Thread.Sleep(6 * 1000);

            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Additional Settings", 210, 1128);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
            if (err != 0) return err;
            Thread.Sleep(6 * 1000);
            for (int i = 0; i < 5; i++)
            {
                //Backup and Reset
                ret = tapScreenByNode(adb, serialno, "Back Up and Reset", 72, 1619);
                if (ret == 0)
                {
                    break;
                }
           
                if (ret == 4)
                {
                    adbHelper.LogIt(string.Format("CPH1875 can't find Back Up and Reset button.start find \"Backup and Reset\""));
                    ret = tapScreenByNode(adb, serialno, "Backup and Reset", 72,1923);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }

                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);

            for (int i = 0; i < 5; i++)
            {
                //Reset to Factory Settings
                ret = tapScreenByNode(adb, serialno, "Erase all data (factory reset)", 72, 1640);
                if (ret == 0)
                {
                    break;
                }

                if (ret == 4)
                {
                    adbHelper.LogIt(string.Format("CPH1875 can't find Erase all data (factory reset) button.start find \"Reset to Factory Settings\""));
                    ret = tapScreenByNode(adb, serialno, "Reset to Factory Settings", 72, 1558);
                    if (ret == 0 || ret == 4)
                    {
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Data", 72, 681);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1864);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3 * 1000);
            for (int i = 0; i < 5; i++)
            {
                int r = tapScreenByNode(adb, serialno, "Erase Data", 0, 1864);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);

            return ret;
        }

        static int do_FactoryResetOPPO(adbHelper adb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryResetOPPO: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            string[] lines;
            // remove the DeviceAdmin popup message
            ret = tapScreenByNode(adb, serialno, "OK", 50, 900);

            // start activity
            lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
            if (err != 0) return err;
            Thread.Sleep(5*1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Reset to Factory Settings", 48, 1040);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3*1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase All Data", 48, 425);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3*1000);
            for (int i = 0; i < 5; i++)
            {
                ret = tapScreenByNode(adb, serialno, "Erase Data", 0, 1258);
                if (ret == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            if (ret != 0) return ret;
            Thread.Sleep(3*1000);
            for (int i = 0; i < 5; i++)
            {
                int r =  tapScreenByNode(adb, serialno, "Erase Data", 0, 1258);
                if (r == 0 || ret == 4)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);
            //tapScreenByNode(adb, serialno, "//node[@text='Erase Data']", 0, 1258);

            return ret;
        }

        static int do_FactoryReset(StringDictionary sarg, adbHelper adb, StringBuilder sb, String sID)
        {
            int ret = 1;
            int err = 0;
            adbHelper.LogIt("do_FactoryReset: ++");
            string serialno = string.IsNullOrEmpty(sID) ? "" : $"-s {sID}";
            // 1. get device properties
            string[] lines = adbHelper.runExe_v2(adb.GetAdbExe(), $"{serialno} shell getprop", out err);
            // convert lines into string dictionary
            StringDictionary props = new StringDictionary();
            foreach (var l in lines)
            {
                int pos = l.IndexOf(':');
                if (pos > 0 && pos < l.Length)
                {
                    string k = l.Substring(0, pos);
                    string v = l.Substring(pos + 1);
                    props[k.Trim(new char[] { '[', ']', ' ' })] = v.Trim(new char[] { '[', ']', ' ' });
                }
            }
            // check [ro.product.manufacturer]: [HUAWEI] and [ro.product.model]: [PE-TL10]
            string ro_product_manufacturer = props["ro.product.manufacturer"];
            string ro_product_model = props["ro.product.model"];
            int com_android_settings_version = 0;
            adbHelper.LogIt($"do_FactoryReset: ro.product.manufacturer={ro_product_manufacturer}");
            adbHelper.LogIt($"do_FactoryReset: ro.product.model={ro_product_model}");

            if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH1943") == 0))
            {
                ret = do_FactoryResetOPPO(adb, sID);
                if (ret != 0)
                {
                    ret = do_FactoryResetOPPOCPH1943(adb, sID);
                }

            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH1983") == 0))
            {
                ret = do_FactoryResetOPPOCPH1983(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH2099") == 0))
            {
                ret = do_FactoryResetOPPOCPH2099(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH2199") == 0))
            {
                ret = do_FactoryResetOPPOCPH2199(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH2135") == 0))
            {
                ret = do_FactoryResetOPPOCPH2135(adb, sID);
            }
            //do_FactoryResetOPPOAdditional
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "OPG02") == 0))
            {
                adbHelper.LogIt($"Model:{ro_product_model} FactoryReset");
                ret = do_FactoryResetOPPOAdditional(adb, sID);
            }
            else if (string.Compare(ro_product_model, "CPH1835") == 0
                || string.Compare(ro_product_model, "CPH1893") == 0
                || string.Compare(ro_product_model, "OPG01") == 0
                || string.Compare(ro_product_model, "CPH2237") == 0
                || string.Compare(ro_product_model, "CPH2091") == 0
                || string.Compare(ro_product_model, "CPH2211") == 0 
                || string.Compare(ro_product_model, "CPH2303") == 0
                || string.Compare(ro_product_model, "A002OP") == 0
                || string.Compare(ro_product_model, "A103OP") == 0
                || string.Compare(ro_product_model, "A101OP") == 0
                || string.Compare(ro_product_model, "CPH2309") == 0
                || string.Compare(ro_product_model, "CPH2251") == 0
                || string.Compare(ro_product_model, "CPH1941") == 0
                || string.Compare(ro_product_model, "CPH2065") == 0
                )
            {
                adbHelper.LogIt($"Model:{ro_product_model} FactoryReset");
                ret = do_FactoryResetOPPOAdditional(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH2195") == 0))
            {
                ret = do_FactoryResetOPPOCPH2195(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "A001OP") == 0))
            {
                ret = do_FactoryResetOPPOA001OP(adb, sID);
            }
//             else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "A101OP") == 0))
//             {
//                 ret = do_FactoryResetOPPOA101OP(adb, sID);
//             }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH1877") == 0))
            {
                ret = do_FactoryResetOPPOCPH1877(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH1919") == 0))
            {
                ret = do_FactoryResetOPPOCPH1919(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH1875") == 0))
            {
                ret = do_FactoryResetOPPOCPH1875(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH1903") == 0))
            {
                ret = do_FactoryResetOPPOCPH1903(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH1851") == 0))
            {
                ret = do_FactoryResetOPPOCPH1851(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "OPPO") == 0 && string.Compare(ro_product_model, "CPH2013") == 0))
            {
                ret = do_FactoryResetOPPOCPH2013(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "HUAWEI") == 0 && string.Compare(ro_product_model, "ALE-UL00") == 0))
            {
                ret = do_FactoryResetHuaweiALEUL00(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "HUAWEI") == 0 && string.Compare(ro_product_model, "ALE-L21") == 0))
            {
                ret = do_FactoryResetHuaweiALEL21(adb, sID);
            }
            else if ((string.Compare(ro_product_manufacturer, "HUAWEI") == 0 && string.Compare(ro_product_model, "PE-TL10") == 0))
            {
                ret = do_FactoryResetHuaweiPETL10(adb, sID);
                // get version code of com.android.settings
                //lines = adbHelper.runExe_v2(adb.GetAdbExe(), $"{serialno} shell pm list packages -s --show-versioncode", out err);
                //foreach(var l in lines)
                //{
                //    if (l.StartsWith("package:com.android.settings"))
                //    {
                //        int pos = l.IndexOf("versionCode:");
                //        if(pos>0 && pos + 12 < l.Length)
                //        {
                //            string s = l.Substring(pos + 12);
                //            if(!Int32.TryParse(s, out com_android_settings_version))
                //            {
                //                com_android_settings_version = 0;
                //            }
                //        }
                //    }
                //}
                // start am 
                //if (com_android_settings_version == 26)
                //                 {
                //                     // start activity
                //                     try
                //                     {
                //                         lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell am start -a android.settings.BACKUP_AND_RESET_SETTINGS com.android.settings/.Settings\$PrivacySettingsActivity", out err);
                //                         lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
                //                         string s = System.IO.Path.GetRandomFileName();
                //                         s = System.IO.Path.ChangeExtension(s, ".xml");
                //                         lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell uiautomator dump /sdcard/{s}", out err);
                //                         lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} pull /sdcard/{s} {System.IO.Path.Combine(System.IO.Path.GetTempPath(), s)}", out err);
                //                         XmlDocument doc = new XmlDocument();
                //                         doc.Load(System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
                //                         XmlNode n = doc.SelectSingleNode("//node[@text='Factory data reset']");
                //                         if (n != null)
                //                         {
                //                             XmlAttribute a = n.Attributes["bounds"];
                //                             if (a != null)
                //                             {
                //                                 Regex r = new Regex(@"\[(\d+),(\d+)\]\[(\d+),(\d+)\]");
                //                                 Match m = r.Match(a.Value);
                //                                 if (m.Success)
                //                                 {
                //                                     int x = -1, y = -1;
                //                                     if(Int32.TryParse(m.Groups[1].Value, out x) && Int32.TryParse(m.Groups[2].Value, out y))
                //                                     {
                //                                         lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {x+10} {y+10}", out err);
                //                                     }
                //                                 }
                // 
                //                             }
                //                         }
                //                         else
                //                         {
                //                             adbHelper.LogIt($"do_FactoryReset: not found Factory data reset!");
                //                             adbHelper.LogIt(System.IO.File.ReadAllText(s));
                //                         }
                //                         System.IO.File.Delete(System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
                //                     }
                //                     catch (Exception) { }
                //                     // second page, check checkbox and reset
                //                     try
                //                     {
                //                         lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input swipe 500 1000 500 100", out err);
                //                         {
                //                             string s = System.IO.Path.GetRandomFileName();
                //                             s = System.IO.Path.ChangeExtension(s, ".xml");
                //                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell uiautomator dump /sdcard/{s}", out err);
                //                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} pull /sdcard/{s} {System.IO.Path.Combine(System.IO.Path.GetTempPath(), s)}", out err);
                //                             XmlDocument doc = new XmlDocument();
                //                             doc.Load(System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
                //                             XmlNodeList nodes = doc.SelectNodes("//node[@class='android.widget.CheckBox']");
                //                             foreach (XmlNode n in nodes)
                //                             {
                //                                 XmlAttribute a = n.Attributes["bounds"];
                //                                 if (a != null)
                //                                 {
                //                                     Regex r = new Regex(@"\[(\d+),(\d+)\]\[(\d+),(\d+)\]");
                //                                     Match m = r.Match(a.Value);
                //                                     if (m.Success)
                //                                     {
                //                                         int x = -1, y = -1;
                //                                         if (Int32.TryParse(m.Groups[1].Value, out x) && Int32.TryParse(m.Groups[2].Value, out y))
                //                                         {
                //                                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {x + 10} {y + 10}", out err);
                //                                         }
                //                                     }
                //                                 }
                //                             }
                //                             System.IO.File.Delete(s);
                //                         }
                //                         //
                //                         {
                //                             string s = System.IO.Path.GetRandomFileName();
                //                             s = System.IO.Path.ChangeExtension(s, ".xml");
                //                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell uiautomator dump /sdcard/{s}", out err);
                //                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} pull /sdcard/{s} {System.IO.Path.Combine(System.IO.Path.GetTempPath(), s)}", out err);
                //                             XmlDocument doc = new XmlDocument();
                //                             doc.Load(System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
                //                             XmlNode n = doc.SelectSingleNode("//node[@resource-id='com.android.settings:id/initiate_master_clear']");
                //                             if(n!=null)
                //                             {
                //                                 XmlAttribute a = n.Attributes["bounds"];
                //                                 if (a != null)
                //                                 {
                //                                     Regex r = new Regex(@"\[(\d+),(\d+)\]\[(\d+),(\d+)\]");
                //                                     Match m = r.Match(a.Value);
                //                                     if (m.Success)
                //                                     {
                //                                         int x = -1, y = -1;
                //                                         if (Int32.TryParse(m.Groups[1].Value, out x) && Int32.TryParse(m.Groups[2].Value, out y))
                //                                         {
                //                                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {x + 10} {y + 10}", out err);
                //                                         }
                //                                     }
                //                                 }
                //                             }
                //                             else
                //                             {
                //                                 adbHelper.LogIt($"do_FactoryReset: not found Factory data reset!");
                //                                 adbHelper.LogIt(System.IO.File.ReadAllText(s));
                //                             }
                //                             System.IO.File.Delete(s);
                //                         }
                //                     }
                //                     catch (Exception) { }
                // 
                //                     // third page
                //                     try
                //                     {
                //                         {
                //                             string s = System.IO.Path.GetRandomFileName();
                //                             s = System.IO.Path.ChangeExtension(s, ".xml");
                //                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell uiautomator dump /sdcard/{s}", out err);
                //                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} pull /sdcard/{s} {System.IO.Path.Combine(System.IO.Path.GetTempPath(), s)}", out err);
                //                             XmlDocument doc = new XmlDocument();
                //                             doc.Load(System.IO.Path.Combine(System.IO.Path.GetTempPath(), s));
                //                             XmlNode n = doc.SelectSingleNode("//node[@resource-id='com.android.settings:id/execute_master_clear']");
                //                             if (n != null)
                //                             {
                //                                 XmlAttribute a = n.Attributes["bounds"];
                //                                 if (a != null)
                //                                 {
                //                                     Regex r = new Regex(@"\[(\d+),(\d+)\]\[(\d+),(\d+)\]");
                //                                     Match m = r.Match(a.Value);
                //                                     if (m.Success)
                //                                     {
                //                                         int x = -1, y = -1;
                //                                         if (Int32.TryParse(m.Groups[1].Value, out x) && Int32.TryParse(m.Groups[2].Value, out y))
                //                                         {
                //                                             lines = adbHelper.runExe_v2(adb.GetAdbExe(), $@"{serialno} shell input tap {x + 10} {y + 10}", out err);
                //                                         }
                //                                     }
                //                                 }
                //                             }
                //                             else
                //                             {
                //                                 adbHelper.LogIt($"do_FactoryReset: not found Factory data reset!");
                //                                 adbHelper.LogIt(System.IO.File.ReadAllText(s));
                //                             }
                //                             System.IO.File.Delete(s);
                //                         }
                //                     }
                //                     catch (Exception) { }
                // 
                //                     ret = 0;
                //                 }
            }
            else
            {
                ret = ClickFactoryReset(sarg, adb, sb, sID);
            }
            adbHelper.LogIt($"do_FactoryReset: -- ret={ret}");
            return ret;
        }
        static int ClickFactoryReset(StringDictionary sarg, adbHelper adb, StringBuilder sb, String sID)
        {
            /*
             * adb push E:\Works\Eclipse\Demos\UiAutomatorDemo\app\build\outputs\apk\debug\app-debug.apk /data/local/tmp/com.fd.uiautomatordemo
adb shell pm install -t -r "/data/local/tmp/com.fd.uiautomatordemo"
[adb shell am force-stop com.fd.uiautomatordemo]

adb push E:\Works\Eclipse\Demos\UiAutomatorDemo\app\build\outputs\apk\androidTest\debug\app-debug-androidTest.apk /data/local/tmp/com.fd.uiautomatordemo.test
adb shell pm install -t -r "/data/local/tmp/com.fd.uiautomatordemo.test"
adb shell am instrument -w -r   -e debug false -e class com.fd.uiautomatordemo.UiTest com.fd.uiautomatordemo.test/android.support.test.runner.AndroidJUnitRunner
             */
            int ret = 1;
            List<String> cmds = new List<string>();
            List<String> tempfiles = new List<string>();

            String sFileName = Path.GetTempFileName();
            File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.app);
            //adb push E:\Works\Eclipse\Demos\UiAutomatorDemo\app\build\outputs\apk\debug\app-debug.apk /data/local/tmp/com.fd.uiautomatordemo
            string ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/com.fd.uiautomatordemo", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
            cmds.Add(ssm);
            tempfiles.Add(sFileName);
            

            sFileName = Path.GetTempFileName();
            File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.app_androidTest);
            //adb push E:\Works\Eclipse\Demos\UiAutomatorDemo\app\build\outputs\apk\debug\app-debug.apk /data/local/tmp/com.fd.uiautomatordemo
            ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/com.fd.uiautomatordemo.test", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
            cmds.Add(ssm);
            tempfiles.Add(sFileName);

            cmds.Add(String.Format(" {0} shell pm install -t -r /data/local/tmp/com.fd.uiautomatordemo", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));
           // cmds.Add(String.Format(" {0} shell am force-stop com.fd.uiautomatordemo", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));
            cmds.Add(String.Format(" {0} shell pm install -t -r /data/local/tmp/com.fd.uiautomatordemo.test", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));

            //adb.runAdb(ssm);
            foreach (String cmd in cmds)
            {
                adbHelper.LogIt(cmd);
                if (adb.runAdb(cmd) != ERRORCODE.SUCCESS)
                {
                    sb.Append("M");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                sb.Append("N");
            }
            foreach (String filname in tempfiles)
            {
                try
                {
                    File.Delete(filname);
                }
                catch (Exception)
                {
                }
            }

            ssm = String.Format(" {0} shell am instrument -w -r   -e debug false -e class com.fd.uiautomatordemo.UiTest com.fd.uiautomatordemo.test/android.support.test.runner.AndroidJUnitRunner", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
            if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
            {
                sb.Append("O_");
                Console.WriteLine(sb);
                return ERRORCODE.ERROR_ADBRUNFAILED;
            }

            string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            StringDictionary sd = new StringDictionary();
            sd["factoryreset"] = "false";
            foreach (string s in sarray)
            {
                adbHelper.LogIt(s);
                if (s.IndexOf("[RESULT]=factoryreset=success") > 0)
                {
                    sd["factoryreset"] = "true";
                    ret = ERRORCODE.SUCCESS;
                    break;
                }

            }
            foreach (DictionaryEntry value in sd)
            {
                Console.WriteLine(value.Key + "=" + value.Value);
            }
            Console.WriteLine(sb);

            return ret;
        }

        static int Main(string[] args)
        {//-utilpath=%apsthome%\adb.exe -serial=TA9921CUVC
         // LastFlowChar=L
            adbHelper.LogIt(String.Format("{0} start: ++ version: {1}",
                Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName),
                Process.GetCurrentProcess().MainModule.FileVersionInfo.FileVersion));
            foreach(string s in args)
            {
                adbHelper.LogIt(s);
            }

            System.Configuration.Install.InstallContext _param = new System.Configuration.Install.InstallContext(null, args);
            String sADB = "";
            StringBuilder sb = new StringBuilder("flowchart=");
            Boolean IsSamsung = false;
            Boolean IsLG = false;
            Boolean IsScreenLock = true;
            Boolean isCheckScreenLock = false;
            if (_param.IsParameterTrue("help"))
            {
                Usage();
                return ERRORCODE.SUCCESS;
            }
            if (_param.Parameters.ContainsKey("utilpath") && !String.IsNullOrEmpty(_param.Parameters["utilpath"]))
            {
                sb.Append("a");
                sADB = System.Environment.ExpandEnvironmentVariables(_param.Parameters["utilpath"].Trim());
            }
            else
            {
                sb.Append("b");
                sADB = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "adb.exe");
            }

            if(!System.IO.File.Exists(sADB))
            {
                adbHelper.LogIt("adb not found.");
                sb.Append("c");
                Console.WriteLine(sb);
                return ERRORCODE.ERROR_NOADB;
            }

            if (_param.Parameters.ContainsKey("specialkey") && !String.IsNullOrEmpty(_param.Parameters["specialkey"]))
            {
                isCheckScreenLock = GetCheckScreenLock(_param.Parameters["specialkey"]);
            }

            if (_param.Parameters.ContainsKey("label") && !String.IsNullOrEmpty(_param.Parameters["label"]))
            {
                sb.Append("d");
                try
                {
                    adbHelper._label = Convert.ToInt32(_param.Parameters["label"].Trim());
                }
                catch(Exception)
                {

                }

            }
            if (_param.Parameters.ContainsKey("screenlock") && !String.IsNullOrEmpty(_param.Parameters["screenlock"]))
            {
                sb.Append("e");
                try
                {
                    IsScreenLock = Convert.ToBoolean(_param.Parameters["screenlock"].Trim());
                }
                catch(Exception)
                {

                }
            }
            string sID = "";
            if (_param.Parameters.ContainsKey("serial") && !String.IsNullOrEmpty(_param.Parameters["serial"]))
            {
                sb.Append("f");
                sID = _param.Parameters["serial"].Trim();
            }
            if (_param.Parameters.ContainsKey("manufacture") && !String.IsNullOrEmpty(_param.Parameters["manufacture"]))
            {
                sb.Append("g");
                if(_param.Parameters["manufacture"].IndexOf("samsung", StringComparison.CurrentCultureIgnoreCase)>=0)
                {
                    sb.Append("h1");
                    IsSamsung = true;
                }
                else if (_param.Parameters["manufacture"].IndexOf("lg", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    sb.Append("h2");
                    IsLG = true;
                }
            }

            adbHelper adb = new adbHelper(sADB);

            if (_param.IsParameterTrue("setoemunlock"))
            {
                String sFileName = Path.GetTempFileName();
                File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.SecuritySettings);
                string ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/SecuritySettings.jar", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
                //adb.runAdb(ssm);
                adbHelper.LogIt(ssm);
                if (adb.runAdb(ssm) != ERRORCODE.SUCCESS)
                {
                    sb.Append("M");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                sb.Append("N");
                try
                {
                    File.Delete(sFileName);
                }
                catch (Exception)
                {
                }

                ssm = String.Format(" {0} shell uiautomator runtest /data/local/tmp/SecuritySettings.jar -c com.fd.fma.SecuritySettings#testSetOemUnlock", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
                if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
                {
                    sb.Append("O_");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                
                string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                StringDictionary sd = new StringDictionary();
                sd.Add("oemremove", "false");
                foreach (string s in sarray)
                {
                    string[] infos = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    if (infos != null && infos.Length == 2)
                    {
                        if (string.Compare(infos[0], "oemunlock", true) == 0 && string.Compare(infos[1], "clear", true) == 0)
                        {
                            //Console.WriteLine("oemremove=true");
                            sd["oemremove"] = "true";
                            break;
                        }
                        else if (string.Compare(infos[0], "oemunlock", true) == 0 && string.Compare(infos[1], "notfindcontroller", true) == 0)
                        {
                            //Console.WriteLine("oemremove=notfindcontroller");
                            sd["oemremove"] = "notfindcontroller";
                            break;
                        }
                        else if (string.Compare(infos[0], "oemunlock", true) == 0 && string.Compare(infos[1], "checked", true) == 0)
                        {
                           // Console.WriteLine("oemremove=true");
                            sd["oemremove"] = "true";
                            break;
                        }
                    }
                }
                foreach (DictionaryEntry value in sd)
                {
                    Console.WriteLine(value.Key +"="+value.Value);
                }
                return ERRORCODE.SUCCESS;

            }
            else if (_param.IsParameterTrue("removegoogleaccount"))
            {
                Boolean bFinddel = false;
                if (GoogleAccount.RemoveUI(_param.Parameters, adb, sb, sID) == ERRORCODE.SUCCESS)
                {
                    Console.WriteLine("oemremove=true");
                    bFinddel = true;
                    return ERRORCODE.SUCCESS;
                }

                if (GoogleAccount.Remove90(_param.Parameters, adb, sb, sID) == ERRORCODE.SUCCESS)
                {
                    Console.WriteLine("oemremove=true");
                    bFinddel = true;
                    return ERRORCODE.SUCCESS;
                }

                String sFileName = Path.GetTempFileName();
                File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.SecuritySettings);
                string ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/SecuritySettings.jar", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
                //adb.runAdb(ssm);
                adbHelper.LogIt(ssm);
                if (adb.runAdb(ssm) != ERRORCODE.SUCCESS)
                {
                    sb.Append("M");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                sb.Append("N");
                try
                {
                    File.Delete(sFileName);
                }
                catch (Exception)
                {
                }
                ssm = String.Format(" {0} shell uiautomator runtest /data/local/tmp/SecuritySettings.jar -c com.fd.fma.SecuritySettings#testRemoveGoogleAccount", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
                if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
                {
                    sb.Append("O1_");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in sarray)
                {
                    string[] infos = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    if (infos != null && infos.Length == 2)
                    {
                        if (string.Compare(infos[0], "oemunlock", true) == 0 && string.Compare(infos[1], "clear", true) == 0)
                        {
                            Console.WriteLine("oemremove=true");
                            bFinddel = true;
                            break;
                        }else if (string.Compare(infos[0], "xypos", true) == 0)
                        {
                            Console.WriteLine("oemremove=true");
                            bFinddel = true;
                            break;
                        }
                    }
                }
                if (!bFinddel)
                {
                    ssm = String.Format(" {0} shell uiautomator runtest /data/local/tmp/SecuritySettings.jar -c com.fd.fma.SecuritySettings#testRemoveGoogleAccount80", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
                    if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
                    {
                        sb.Append("O1_");
                        Console.WriteLine(sb);
                        return ERRORCODE.ERROR_ADBRUNFAILED;
                    }
                    sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string s in sarray)
                    {
                        string[] infos = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        if (infos != null && infos.Length == 2)
                        {
                            if (string.Compare(infos[0], "oemunlock", true) == 0 && string.Compare(infos[1], "clear", true) == 0)
                            {
                                Console.WriteLine("oemremove=true");
                                bFinddel = true;
                                break;
                            }
                        }
                    }
                }
                
                return ERRORCODE.SUCCESS;
            }

            if(_param.IsParameterTrue("factoryreset"))
            {
                //return ClickFactoryReset(_param.Parameters, adb, sb, sID);
                return do_FactoryReset(_param.Parameters, adb, sb, sID);
            }

            if(_param.IsParameterTrue("screenon"))
            {
                String sFileName = Path.GetTempFileName();
                File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.SecuritySettings);
                string ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/SecuritySettings.jar", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
                //adb.runAdb(ssm);
                adbHelper.LogIt(ssm);
                if (adb.runAdb(ssm) != ERRORCODE.SUCCESS)
                {
                    sb.Append("M");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                sb.Append("N");
                try
                {
                    File.Delete(sFileName);
                }
                catch (Exception)
                {
                }

                ssm = String.Format(" {0} shell uiautomator runtest /data/local/tmp/SecuritySettings.jar -c com.fd.fma.SecuritySettings#testScreenOn", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
               
                if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
                {
                    sb.Append("O");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                adb.SetAdbExe(sADB);
                foreach (string s in sarray)
                {
                    string[] infos = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    if (infos != null && infos.Length == 2)
                    {
                       // if (string.Compare(infos[0], "language", true) == 0)
                        {
                            Console.WriteLine(s);
                           // break;
                        }
                    }
                }
                sb.Append("P");
                SendCurlInfo(true);
                sb.Append("Q");
                Console.WriteLine(sb);
                return ERRORCODE.SUCCESS;
            }

            if (_param.IsParameterTrue("setlanguage") || _param.Parameters.ContainsKey("setlanguage"))
            {
                String sFileName = Path.GetTempFileName();
                File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.SecuritySettings);
                string ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/SecuritySettings.jar", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
                //adb.runAdb(ssm);
                adbHelper.LogIt(ssm);
                if (adb.runAdb(ssm) != ERRORCODE.SUCCESS)
                {
                    sb.Append("M");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                sb.Append("N");
                try
                {
                    File.Delete(sFileName);
                }
                catch (Exception)
                {
                }

                ssm = String.Format(" {0} shell uiautomator runtest /data/local/tmp/SecuritySettings.jar -c com.fd.fma.SecuritySettings#testSetEnglish", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
                if (_param.Parameters.ContainsKey("setlanguage") && !String.IsNullOrEmpty(_param.Parameters["setlanguage"]))
                {
                    ssm = String.Format(" {0} -e language {1}", ssm, _param.Parameters["setlanguage"]);
                }
                if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
                {
                    sb.Append("O");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                adb.SetAdbExe(sADB);
                foreach (string s in sarray)
                {
                    string[] infos = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    if (infos != null && infos.Length == 2)
                    {
                        if (string.Compare(infos[0], "language", true) == 0)
                        {
                            Console.WriteLine(s);
                            break;
                        }
                    }
                }
                return ERRORCODE.SUCCESS;
            }

            if (_param.IsParameterTrue("knoxbit"))
            {
                if (IsSamsung)
                {
                    Dictionary<String, String> dict = adbHelper.getDeviceProp(sADB, "", sID);
                    String[] knoxbitfalse = { "0", "unknown" };
                    if (dict.ContainsKey(ReadFMA.Properties.Resources.knoxbit))
                    {
                        Console.WriteLine("knoxbit=" + ((Array.IndexOf(knoxbitfalse, dict[ReadFMA.Properties.Resources.knoxbit]) == -1) ? "true" : "false"));
                    }
                    else
                    {
                        Console.WriteLine("knoxbit=false");
                    }
                    return ERRORCODE.SUCCESS;
                }
                else
                    return ERRORCODE.ERROR_NO_TSUPPORTED;
            }

            if(_param.IsParameterTrue("mcafee"))
            {
                if(IsLG)
                {
                    return CheckMcafeeLock(sb, adb, sID);
                }
                else
                    return ERRORCODE.ERROR_NO_TSUPPORTED;
            }

            if (IsSamsung)
            {
                sb.Append("i");
                String sHubName = _param.Parameters.ContainsKey("hubname") ? _param.Parameters["hubname"].Trim() : "";
                String sHubport = _param.Parameters.ContainsKey("hubport") ? _param.Parameters["hubport"].Trim() : "";
                string samsungatexe = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "SamsungAndroid.exe");
                //	-hubname=   connect hub Name.	-hubport=   connect hub port.	-reactive  Samsung device reactive status info	-label=x  
                if (!File.Exists(samsungatexe)) {
                    sb.Append("j");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_NOSAMSUNGANDROID;
                }
                String sParamreactive = string.Format("-hubname={0}	-hubport={1} -reactive -label={2}", sHubName, sHubport, adbHelper._label);
                //call exe
                adb.runExe(samsungatexe, sParamreactive, System.AppDomain.CurrentDomain.BaseDirectory);
                string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                sb.Append("k");
                adb.SetAdbExe(sADB);
                foreach (string s in sarray)
                {
                    string[] infos = s.Split(new char[] { '='}, StringSplitOptions.RemoveEmptyEntries);
                    if (infos != null && infos.Length == 2)
                    {
                        if (string.Compare(infos[0], "REACTIVE", true) == 0)
                        {
                            sb.Append("l");
                            if(string.Compare(infos[1], "LOCK", true) == 0)
                            {
                                if (String.Compare(_param.Parameters.ContainsKey("FMM")?_param.Parameters["FMM"].Trim():"", "true", true) == 0)
                                {
                                    sb.Append("m");
                                    Console.WriteLine("FMM=true");
                                }
                                else
                                {
                                    sb.Append("n");
                                    Console.WriteLine("FRP=true");
                                }
                                Console.WriteLine(sb);
                                return ERRORCODE.SUCCESS;
                            }
                            else if (string.Compare(infos[1], "UNLOCK", true) == 0)
                            {
                                sb.Append("o");
                                Console.WriteLine("FRP=false");
                                Console.WriteLine(sb);
                                return ERRORCODE.SUCCESS;
                            }
                        }
                    }
                }
                if (String.Compare(_param.Parameters.ContainsKey("FMM") ? _param.Parameters["FMM"].Trim() : "", "true", true) == 0)
                {
                    sb.Append("p");
                    Console.WriteLine("FMM=true");
                    Console.WriteLine(sb);
                    return ERRORCODE.SUCCESS;
                }
            }

            if (_param.Parameters.ContainsKey("googlecount"))
            {
                sb.Append("q");
                String sgoogleCount = _param.Parameters["googlecount"].Trim();
                if (Convert.ToInt32(sgoogleCount) < 1)
                {
                    sb.Append("r");
                    Console.WriteLine("FRP=false");
                    Console.WriteLine(sb);
                    return ERRORCODE.SUCCESS;
                }
            }
            else
            {
                sb.Append("s");
                Console.WriteLine("FRP=false");
                Console.WriteLine(sb);
                return ERRORCODE.SUCCESS;
            }

            if (_param.Parameters.ContainsKey("version"))
            {
                sb.Append("t");
                char[] SeparatorsArray = new char[] { '.' };
                String vs = _param.Parameters["version"].Trim();
                String[] parsedComponents = vs.Split(SeparatorsArray);
                StringBuilder sbver = new StringBuilder();
                int parsedComponentsLength = parsedComponents.Length;
                for (int i = 0; i < 3; i++)
                {
                    sbver.Append(i < parsedComponentsLength ? parsedComponents[i] : "0").Append(".");
                }
                sbver.Append(3 < parsedComponentsLength ? parsedComponents[3] : "0");
                Version ver = new Version(sbver.ToString());
                if (ver.CompareTo(new Version(5, 1)) < 0)
                {
                    sb.Append("u");
                    Console.WriteLine("FRP=false");
                    Console.WriteLine(sb);
                    return ERRORCODE.SUCCESS;
                }
                else
                {
                    sb.Append("v");
                    bool? oemunlock = null;
                    string sram = String.Format(" {0} shell getprop ro.frp.pst", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
                    if (adb.runAdb(sram) != ERRORCODE.SUCCESS)
                    {
                        sb.Append("J");
                        Console.WriteLine(sb);
                        return ERRORCODE.ERROR_ADBRUNFAILED;
                    }
                    string ss = adb.getAdbOutput();
                    adbHelper.LogIt(ss);
                    if (ss.Trim().Length > 0 && ss.IndexOf("/") >= 0)
                    {
                        sb.Append("w");
                        Console.WriteLine("frpsupport=true");
                        Console.WriteLine("frppath=" + ss);
                    }
                    else
                    {
                        sb.Append("x");
                        Console.WriteLine("FRP=false");
                        Console.WriteLine(sb);
                        return ERRORCODE.SUCCESS;
                    }

                    String sFileName = Path.GetTempFileName();
                    File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.SecuritySettings);
                    string ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/SecuritySettings.jar", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
                    //adb.runAdb(ssm);
                    adbHelper.LogIt(ssm);
                    if (adb.runAdb(ssm) != ERRORCODE.SUCCESS)
                    {
                        sb.Append("K");
                        Console.WriteLine(sb);
                        return ERRORCODE.ERROR_ADBRUNFAILED;
                    }
                    sb.Append("y");
                    try
                    {
                        File.Delete(sFileName);
                    }
                    catch (Exception)
                    {
                    }
                    int iRetry = 0;
retryOemunlock:
                    ssm = String.Format(" {0} shell uiautomator runtest /data/local/tmp/SecuritySettings.jar -c com.fd.fma.SecuritySettings#testOemUnlock", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
                    if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
                    {
                        sb.Append("L");
                        Console.WriteLine(sb);
                        return ERRORCODE.ERROR_ADBRUNFAILED;
                    }
                    string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                    sb.Append("z");
                    StringDictionary stretmap = new StringDictionary();
                    foreach (string s in sarray)
                    {
                        string[] infos = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        if (infos != null && infos.Length == 2)
                        {
                            stretmap[infos[0].ToLower()] = infos[1];
                        }
                    }

                    
                    if (stretmap.ContainsKey("error"))
                    {
                        sb.Append("D");
                        adbHelper.LogIt("error is found.");
                        oemunlock = false;
                        if (string.Compare(stretmap["error"], "retry", true) == 0 && iRetry++ < 3)
                            goto retryOemunlock;
                    }
                    else if (stretmap.ContainsKey("exception"))
                    {
                        sb.Append("E");
                        oemunlock = false;
                        adbHelper.LogIt("exception is found.");
                    }
                    else if (stretmap.ContainsKey("oemunlock"))
                    {
                        sb.Append("A");
                        if (string.Compare(stretmap["oemunlock"], "false", true) == 0 )
                        {
                            sb.Append("B");
                            Console.WriteLine("FRP=false");
                            Console.WriteLine(sb);
                            return ERRORCODE.SUCCESS;
                        }
                        else //no or false  //|| string.Compare(infos[1], "no", true) == 0  mean not find controller
                        {
                            sb.Append("C");
                            oemunlock = false;
                        }
                    }

                    if (!oemunlock.HasValue)
                    {
                        sb.Append("F");
                        Console.WriteLine(sb);
                        Console.WriteLine("FRP=true");
                        return ERRORCODE.ERROR_OEMUNLOCK;
                    }
                    if (!isCheckScreenLock || IsScreenLock)
                    {
                        sb.Append("G");
                        Console.WriteLine("FRP=true");
                        Console.WriteLine(sb);
                        return ERRORCODE.SUCCESS;
                    }
                    else
                    {
                        sb.Append("H");
                        Console.WriteLine("FRP=false");
                        Console.WriteLine(sb);
                        return ERRORCODE.SUCCESS;
                    }

                }
            }
            else
            {
                sb.Append("I");
                Console.WriteLine(sb);
                return ERRORCODE.ERROR_NOVERSION;
            }

        }

    }
}
