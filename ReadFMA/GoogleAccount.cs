﻿using androidmonkey;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;

namespace ReadFMA
{
    class GoogleAccount
    {
        public static bool bimageview = false; //More option
        public static int Remove90(StringDictionary sarg, adbHelper adb, StringBuilder sb, String sID)
        {
            /*
             * adb shell am force-stop com.fd.uiautomatordemo
$ adb push E:\Works\Eclipse\Demos\UiAutomatorDemo\app\build\outputs\apk\androidTest\debug\app-debug-androidTest.apk /data/local/tmp/com.fd.uiautomatordemo.test
$ adb shell pm install -t -r "/data/local/tmp/com.fd.uiautomatordemo.test"
$ adb shell am instrument -w -r   -e debug false -e class com.fd.uiautomatordemo.AccountsRemove com.fd.uiautomatordemo.test/android.support.test.runner.AndroidJUnitRunner
             */

            int ret = 1;
            List<String> cmds = new List<string>();
            List<String> tempfiles = new List<string>();

            cmds.Add(String.Format(" {0} shell am force-stop com.fd.uiautomatordemo", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));
            cmds.Add(String.Format(" {0} shell am force-stop com.fd.uiautomatordemo.test", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));//adb shell am force-stop com.fd.uiautomatordemo.test
            String sFileName = Path.GetTempFileName();
            File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.app);
            //adb push E:\Works\Eclipse\Demos\UiAutomatorDemo\app\build\outputs\apk\debug\app-debug.apk /data/local/tmp/com.fd.uiautomatordemo
            string ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/com.fd.uiautomatordemo", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
            cmds.Add(ssm);
            tempfiles.Add(sFileName);


            sFileName = Path.GetTempFileName();
            File.WriteAllBytes(sFileName, ReadFMA.Properties.Resources.app_androidTest);
            //adb push E:\Works\Eclipse\Demos\UiAutomatorDemo\app\build\outputs\apk\debug\app-debug.apk /data/local/tmp/com.fd.uiautomatordemo
            ssm = String.Format(" {0} push \"{1}\" /data/local/tmp/com.fd.uiautomatordemo.test", String.IsNullOrEmpty(sID) ? "" : "-s " + sID, sFileName);
            cmds.Add(ssm);
            tempfiles.Add(sFileName);

            cmds.Add(String.Format(" {0} shell pm install -t -r /data/local/tmp/com.fd.uiautomatordemo", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));
            // cmds.Add(String.Format(" {0} shell am force-stop com.fd.uiautomatordemo", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));
            cmds.Add(String.Format(" {0} shell pm install -t -r /data/local/tmp/com.fd.uiautomatordemo.test", String.IsNullOrEmpty(sID) ? "" : "-s " + sID));

            //adb.runAdb(ssm);
            foreach (String cmd in cmds)
            {
                adbHelper.LogIt(cmd);
                if (adb.runAdb(cmd, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
                {
                    sb.Append("M");
                    Console.WriteLine(sb);
                    return ERRORCODE.ERROR_ADBRUNFAILED;
                }
                sb.Append("N");
            }
            foreach (String filname in tempfiles)
            {
                try
                {
                    File.Delete(filname);
                }
                catch (Exception)
                {
                }
            }

            ssm = String.Format(" {0} shell am instrument -w -r   -e debug false -e class com.fd.uiautomatordemo.AccountsRemove com.fd.uiautomatordemo.test/android.support.test.runner.AndroidJUnitRunner", String.IsNullOrEmpty(sID) ? "" : "-s " + sID);
            if (adb.runAdb(ssm, '\r', 300 * 1000) != ERRORCODE.SUCCESS)
            {
                sb.Append("O_");
                Console.WriteLine(sb);
                return ERRORCODE.ERROR_ADBRUNFAILED;
            }

            string[] sarray = adb.getAdbOutput().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in sarray)
            {
                if (s.IndexOf("oemunlock=clear") > 0)
                {
                    Console.WriteLine("oemremove=true");
                    ret = ERRORCODE.SUCCESS;
                    break;
                }
            }

            return ret;

        }

        private static int TapDumpXml(StringDictionary sarg, adbHelper adb, String sID, StringBuilder sb, String sParam, String sKey, bool checkbutton = true)
        {
            int ret = 1;
            sb.Append("T+");
            adb.runAdb(sParam, out ret);
            if (ret != 0)
            {
                sb.Append("T-");
                return ret;
            }
            sb.Append("a");
            int xmlpos = adb.getAdbOutput().IndexOf("UI hierchary dumped to");
            if (xmlpos<0)
            {
                sb.Append("l");
                sb.Append("T-");
                return ret;
            }
            String s = System.IO.Path.GetRandomFileName();
            s = System.IO.Path.ChangeExtension(s, ".xml");
            int err = 1;
            String localFile = Path.Combine(Path.GetTempPath(), s);
            adbHelper.runExe_v2(adb.GetAdbExe(), $"{(String.IsNullOrEmpty(sID) ? "" : " -s " + sID)} pull /sdcard/uigoogledump.xml \"{localFile}\"",
                 out err, null, 5 * 60 * 1000);
            if (!File.Exists(localFile))
            {
                sb.Append("m");
                sb.Append("T-");
                return ret;
            }
            adb.runAdb($" {(String.IsNullOrEmpty(sID) ? "" : " -s " + sID)} shell rm /sdcard/uigoogledump.xml", out ret);
            XDocument doc = XDocument.Load(localFile);
            XNamespace ns = doc.Root.GetDefaultNamespace();
            Regex regex = new Regex(sKey, RegexOptions.IgnoreCase);

            var nodes = doc.Descendants(ns + "node")
                           .Where(node1 => regex.IsMatch(node1.Attribute("text").Value));

            sb.Append("c");
            if (nodes.Count() == 0)
            {
                sb.Append("T-");
                adbHelper.LogIt($"not found {sKey}");
                regex = new Regex(@"More options", RegexOptions.IgnoreCase);
                nodes = doc.Descendants(ns + "node")
                           .Where(node1 => regex.IsMatch(node1.Attribute("content-desc").Value));
                if (nodes.Count() == 0)
                {
                    return 2;
                }
                bimageview = true;
            }
            sb.Append("d");
            if (nodes.Count() > 1)
            {
                sb.Append("e");
                adbHelper.LogIt($"found multi {sKey}");
            }
            var node = nodes.First();
            if (checkbutton)
            {
                foreach (var n in nodes)
                {
                    if (bimageview)
                    {
                        if (node.Attribute("class").Value.IndexOf("ImageView", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            node = n;
                            break;
                        }
                    }
                    else
                    {
                        if (node.Attribute("class").Value.IndexOf("button", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            node = n;
                            break;
                        }
                    }
                }
            }


            var attribute = node.Attribute("bounds");
            sb.Append("f");
            if (attribute != null)
            {
                sb.Append("g");
                string position = attribute.Value;
                //[189,1424][743,1501]
                Regex regp = new Regex(@"\[(\d+),(\d+)\]\[(\d+),(\d+)\]");
                Match m = regp.Match(position);
                if (m.Success)
                {
                    sb.Append("h");
                    int x1 = Convert.ToInt32(m.Groups[1].Value);
                    int y1 = Convert.ToInt32(m.Groups[2].Value);
                    int x2 = Convert.ToInt32(m.Groups[3].Value);
                    int y2 = Convert.ToInt32(m.Groups[4].Value);
                    int x = (x1 + x2) / 2;
                    int y = (y1 + y2) / 2;
                    String tapcmd = $" {(String.IsNullOrEmpty(sID) ? "" : " -s " + sID)} shell input tap {x} {y}";
                    sb.Append("i");
                    adb.runAdb(tapcmd, out ret);
                }
            }
            else
            {
                sb.Append("j");
                adbHelper.LogIt("xml format error: not found bounds");
            }
            sb.Append("q");
            try
            {
                if (File.Exists(localFile))
                {
                    File.Delete(localFile);
                }
            }
            catch
            {

            }
            sb.Append("T-");
            return ret;
        }

        public static int RemoveUI(StringDictionary sarg, adbHelper adb, StringBuilder sb, String sID)
        {
            /*   adb shell am start -a  "android.settings.SYNC_SETTINGS"
             * find google account AND CLICK
             */
            sb.Append("R+");
            int ret = 1;
            String startcmd = $" {(String.IsNullOrEmpty(sID) ? "" : " -s " + sID)} shell am start -a  android.settings.SYNC_SETTINGS";
            String startcmdlanguage = $" {(String.IsNullOrEmpty(sID) ? "" : " -s " + sID)} shell cmd locale set-app-locales com.android.settings --locales en-US ";
            String dumpxmlcmd = $" {(String.IsNullOrEmpty(sID) ? "" : " -s " + sID)} shell uiautomator dump /sdcard/uigoogledump.xml";
            //String tapcmd = $" {(String.IsNullOrEmpty(sID) ? "" : " -s " + sID)} shell input tap ";
            int exitcode = 1;
            adb.runAdb(startcmdlanguage, out exitcode);
            Thread.Sleep(1000);
            sb.Append("k");
            for (int i = 0; i < 3; i++)
            {
                adb.runAdb(startcmd, out exitcode);
                if (exitcode != 0)
                {
                    sb.Append("l");
                    adbHelper.LogIt("start activity failed");
                    Thread.Sleep(1000);
                    continue;
                }
                break;
            }
            if (exitcode != 0)
            {
                sb.Append("R-");
                return exitcode;
            }
            sb.Append("m");
            ret = TapDumpXml(sarg, adb, sID, sb, dumpxmlcmd, @".*?@gmail.com", false);
            if (ret == 2)
            {
                adbHelper.LogIt("not find google account");
                sb.Append("R-");
                return 0;
            }
            sb.Append("n");
            ret = TapDumpXml(sarg, adb, sID, sb, dumpxmlcmd, @"^remove account$");
            if (ret != 0)
            {
                adbHelper.LogIt("remove account button failed");
                sb.Append("R-");
                return ret;
            }
            sb.Append("o");
            ret = TapDumpXml(sarg, adb, sID, sb, dumpxmlcmd, @"^remove account$");
            if (ret != 0)
            {
                adbHelper.LogIt("remove account failed");
                sb.Append("R-");
                return ret;
            }

            sb.Append("p");
            if (bimageview)
            {
                ret = TapDumpXml(sarg, adb, sID, sb, dumpxmlcmd, @"^remove account$");
                if (ret != 0)
                {
                    adbHelper.LogIt("remove account failed");
                    sb.Append("R-");
                    return ret;
                }
            }
            sb.Append("R-");
            return ret;
        }
    }
}
