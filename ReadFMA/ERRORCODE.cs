﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReadFMA
{
    class ERRORCODE
    {
        const int ERROR_BASE = 23000;
        public const int ERROR_NOADB = ERROR_BASE + 1; // not find adb.exe
        public const int ERROR_NOSAMSUNGANDROID = ERROR_BASE + 2;// not find SamsungAndroid.exe
        public const int ERROR_OEMUNLOCK = ERROR_BASE + 3; // read oem unlock fail
        public const int ERROR_NOVERSION = ERROR_BASE + 4; // no version info
        public const int ERROR_ADBRUNFAILED = ERROR_BASE + 5; // adb run failed.
        public const int ERROR_TIMEOUT = ERROR_BASE + 1460; // timeout
        public const int ERROR_NO_TSUPPORTED = ERROR_BASE + 50;
        public const int SUCCESS = 0;
    }
}
